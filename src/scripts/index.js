import $ from 'jquery';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/index.scss';

/**
 * Global Constants
 */
const apiKey = '563492ad6f917000010000010ce354f717f04149ac5d9a32f8d5450d';
const baseUrl = 'https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?page=1';

/**
 * Cached Elements
 */
let $container;
let $btnSearch;
let $selectPerPage;

/**
 * Variables
 */
let searchQuery = 'abstract';
let photosPerPage = 15;

/**
 * Wait for the DOM to be ready
 * jQuery needs to cache to DOM :) 
 */
$(document).ready(function () {

    $container = $('#photos');
    $btnSearch = $('#btn-search-images');
    $selectPerPage = $('#photos-per-page');

    // Bind the OnClick event to the Search button.
    $btnSearch.click((e) => {
        searchQuery = $('#searchbox').val();
        fetchImages(searchQuery);
    });

    // Bind the OnChange event to the photos-per-page element.
    $selectPerPage.change((e)=>{
        photosPerPage = e.target.value;
        fetchImages(searchQuery);
    });

    // Execute the fetch function to load the default query.
    fetchImages(searchQuery);

});

/**
 * Fetch the images from the API
 * This function also clears the #photos container of any old photos.
 * @param {string} query 
 */
function fetchImages(query) {
    // Check if there is currently an internet connection.
    if (window.navigator.onLine) {
        $.ajax({
            type: 'GET',
            url: `${baseUrl}&query=${query}&per_page=${photosPerPage}`,
            headers: {
                "Authorization": `Bearer ${apiKey}`
            },
            
        }).done(data => {
            // Clear the previous photos.
            $container.html("");
            renderImages(data.photos);
        });
    }
    else {
        alert("You're not online! 😢");
    }
}

/**
 * Render the created cards into the #photos container.
 * @param [] photos 
 */
function renderImages(photos) {
    $.each(photos, (i, photo) => {
        $container.append(createCard(photo));
    });
}

/**
 * Build a Card template with the photo information.
 * @param {*} photo 
 * @returns String
 */
function createCard(photo) {
    return `
        <div class="col-4">
            <div class="card h-100" style="18rem;">
                <div class="card-img">
                    <img class="card-img-top" src="${photo.src.medium}"/>
                </div>
    
                <div class="card-body">
                    <h5>${photo.photographer}</h5>
                </div>
            </div>
        </div>
    `;
}